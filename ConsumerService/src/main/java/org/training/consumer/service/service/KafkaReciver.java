package org.training.consumer.service.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class KafkaReciver {

	@KafkaListener(topics = "exception", groupId = "group-id")
	public void consume(ConsumerRecord<String, Object> records) {
		
		log.info(records.value()+"");
		
	}

}
