package org.training.producer.service.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.training.producer.service.dto.User;
import org.training.producer.service.service.KafkaSender;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class ProducerController {
	
	private final KafkaSender kafkaSender;
	
	@PostMapping("/sender")
	public void sendMessage(@RequestBody User user) {
		System.out.println(user);
		kafkaSender.send(user);
	}

	
}
